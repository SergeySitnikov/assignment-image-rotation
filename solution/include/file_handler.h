
#ifndef IMAGE_ROTATION_FILE_HANDLER_H
#define IMAGE_ROTATION_FILE_HANDLER_H
#include <stdint.h>
#include "stdio.h"

 enum open_status{
    OPEN_OK,
    READ_ERROR,
    WRITE_FILE_ERROR
};
 enum close_status{
    CLOSE_OK ,
    CLOSE_ERROR
};
FILE* file_read(const char* filepath);
FILE* file_write(const char* filepath);
enum close_status file_close(FILE* open_file);
void print_state_open(enum open_status state );
#endif //IMAGE_ROTATION_FILE_HANDLER_H
