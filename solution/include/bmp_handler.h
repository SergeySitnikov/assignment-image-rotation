
#ifndef IMAGE_ROTATION_BMP_HANDLER_H
#define IMAGE_ROTATION_BMP_HANDLER_H
#include <stdint.h>
#include "stdio.h"
#include "malloc.h"
#include "image_handler.h"

/*  deserializer   */
 enum read_status {
    READ_OK ,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
     READ_INVALID_TYPE,
     FREAD_ERROR,
     READ_FILE_ERROR
    /* more codes */
};
/* serializer */
enum write_status {
    WRITE_OK ,
    WRITE_ERROR,
    NULL_FILE_ERROR
    /* more codes */
};
enum read_status from_bmp( FILE* in, struct image* const read );
enum write_status to_bmp( FILE* out, struct image const* img );
void print_read_status(enum read_status status );
void print_write_status(enum write_status status );
void delete_image(struct image image);
#endif //IMAGE_ROTATION_BMP_HANDLER_H
