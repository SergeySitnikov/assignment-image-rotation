
#ifndef IMAGE_ROTATION_TRANSFORMATION_HANDLER_H
#define IMAGE_ROTATION_TRANSFORMATION_HANDLER_H
#include "image_handler.h"
#include "malloc.h"
struct image rotate( struct image const source );
#endif //IMAGE_ROTATION_TRANSFORMATION_HANDLER_H
