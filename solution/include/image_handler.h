
#ifndef IMAGE_ROTATION_IMAGE_HANDLER_H
#define IMAGE_ROTATION_IMAGE_HANDLER_H
#include <stdint.h>

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct __attribute__((packed)) pixel { uint8_t b, g, r; };
void delete_image(struct image image);
#endif //IMAGE_ROTATION_IMAGE_HANDLER_H
