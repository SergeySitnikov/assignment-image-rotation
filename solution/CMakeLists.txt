cmake_minimum_required(VERSION 3.21)
project(assignment-image-rotation)

set(CMAKE_C_STANDARD 17)
include_directories("solution/include")
add_executable(Image_rotation  solution/src/file_handler.c solution/src/bmp_handler.c  solution/src/transformation_handler.c solution/src/image_handler.c )
