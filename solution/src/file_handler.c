#include "file_handler.h"
// Открываем бинарный файл для чтения
FILE* file_read(const char* filepath){
    FILE* file = fopen(filepath,"rb");
    if(file!=NULL) {
        print_state_open(OPEN_OK);
        return file;
    }
    else {
        print_state_open(READ_ERROR);
        return NULL;
    }
}
// Открываем бинарный файл для записи
FILE* file_write(const char* filepath){
    FILE* file = fopen(filepath,"wb");
    if(file!=NULL) {
       // print_state_open(OPEN_OK);
        return file;
    }
    else {
        print_state_open(WRITE_FILE_ERROR);
        return NULL;
    }
}
//Закрываем файл,если он открытый,иначе выдаем ошибку
 enum close_status file_close(FILE* open_file){
  if(fclose(open_file)){  return CLOSE_OK;}
  else return CLOSE_ERROR;
}
 //Выводим состояние операции
 void print_state_open(enum open_status state ){
     char* message;
     if (state==OPEN_OK){ message=(char *)"File opened successfully";}
     else if (state==READ_ERROR){ message=(char *)"Error reading file";}
     else { message=(char *)"Error writing file";}
     printf("%s\n",message);
 }

