#include "bmp_handler.h"
#include "file_handler.h"
#include "image_handler.h"
#include "transformation_handler.h"
int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning
    if (argc != 3) {printf("Incorrect arguments!");return 0;}
    FILE* file_to_read = file_read(argv[1]);
    if(file_to_read==NULL){fprintf(stderr,"Bad first input file"); file_close(file_to_read);return 0;}
     struct image source_image;
     enum read_status read_status;
    read_status=from_bmp(file_to_read,&source_image);
    if (read_status!=READ_OK) {file_close(file_to_read); return 0;}
    print_read_status(read_status);
    struct image rotate_image=rotate(source_image);
    FILE* file_to_write = file_write(argv[2]);
    if(file_to_write==NULL){file_close(file_to_read);file_close(file_to_write);return 2;}
    enum write_status write_status;
    write_status=to_bmp(file_to_write,&rotate_image);
    if (write_status!=WRITE_OK) {file_close(file_to_read);file_close(file_to_write);return 0;}
    print_write_status(write_status);
    file_close(file_to_read);
    file_close(file_to_write);
    delete_image(source_image);
    delete_image(rotate_image);
    return 0;
}
