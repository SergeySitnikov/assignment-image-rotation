#include "transformation_handler.h"

struct image rotate( struct image const source ) {
    struct image rotate_image;
    rotate_image.width=source.height;
    rotate_image.height=source.width;
    rotate_image.data = malloc(sizeof(struct pixel) * rotate_image.width * rotate_image.height);
    for (size_t i = 0; i < rotate_image.height; i++) {
        for (size_t j = 0; j < rotate_image.width; j++) {
            rotate_image.data[(i*rotate_image.width+j)] = source.data[(source.height-j-1)*source.width+i];
        }
    }
    return rotate_image;
}

