#include "bmp_handler.h"

/* Получаем на вход структуру картинки и инициализируем наш заголовок
  файла с помощью базовых значений и учитываю ширину и высоту картинки*/
struct __attribute__((packed))
bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;//Размер файла
    uint32_t bfReserved;//Зарезервировано
    uint32_t bOffBits;//Смещение изображения от начала файла
    uint32_t biSize;//Длина заголовка
    uint32_t biWidth;//Ширина изображения
    uint32_t biHeight;//Высота изображения
    uint16_t biPlanes;//Число плоскостей
    uint16_t biBitCount;//Глубина цвета, бит на точку
    uint32_t biCompression;//Тип компрессии (0 - несжатое изображение)
    uint32_t biSizeImage;//	Размер изображения, байт
    uint32_t biXPelsPerMeter;//Горизонтальное разрешение, точки на метр
    uint32_t biYPelsPerMeter;//Вертикальное разрешение, точки на метр
    uint32_t biClrUsed;//Число используемых цветов (0 - максимально возможное для данной глубины цвета)
    uint32_t biClrImportant;//Число основных цветов
};
static struct bmp_header initialize_bmp_header(struct  image const* image){
 struct bmp_header bmpHeader ={
         //тип файла 'bm'
         .bfType=0x4D42,
         //размер файла=(размер загаловка+размер изображения)
         .bfileSize=sizeof(struct bmp_header)+image->height * image->width *sizeof(struct pixel)+(image->width % 4)*image->height,
         //bfReserved зарезервиравнно и ложно быть 0
         .bfReserved=0,
         //Очивидно,что изображение от начала файла отделяет загаловок,поэтому в этом поле указываем размер заголовка
         .bOffBits=sizeof(struct bmp_header),
         //Длина заголовка (5 байт =40 бит)
         .biSize=40,
         //ширина изображения
         .biWidth=image->width,
         //длина изображения
         .biHeight=image->height,
         //В нашем случае всегда будет одна плоскость
         .biPlanes=1,
         //глубина цвета 24 бита на пискиль
         .biBitCount=24,
         //Без сжатия
         .biCompression=0,
         //Размер картинки(ширина*высота*размер пикселя + дополняем мусорными байтами до ближайщего кратного четырем)
         .biSizeImage=image->height * image->width *sizeof(struct pixel)+(image->width % 4)*image->height,
         .biXPelsPerMeter=0,
         .biYPelsPerMeter =0,
         //(0-масимально возможное кол-во цветов для данной глубины)
         .biClrUsed=0,
         .biClrImportant=0};
    return bmpHeader;
 }
 static enum  read_status check_bmp_header(struct  bmp_header bmp_header){
     enum read_status result=READ_OK;
     if (bmp_header.biBitCount != 24) { result = READ_INVALID_BITS; return result;}
     else if (bmp_header.bfType != 0x4D42) { result = READ_INVALID_TYPE; return  result;}
     else if (bmp_header.biCompression != 0){result = READ_INVALID_HEADER; return result;}
     else {result=READ_OK;return result;}
}
//Открываем наш bmp файл и вытаскиваем из него картинку
enum read_status from_bmp( FILE* in, struct image* const read_image ) {
    if(!in) return READ_FILE_ERROR;
    enum read_status result;
    struct bmp_header bmp_header;
    //Считываем содержимое файла в нашу выше созданую переменную
    if (!fread(&bmp_header, sizeof(struct bmp_header), 1, in)) return READ_INVALID_TYPE;
    result = check_bmp_header(bmp_header);
    if (result!=READ_OK){ return result;}
    //Задаем параметры из файла нашему изображению
    read_image->height = bmp_header.biHeight;
    read_image->width = bmp_header.biWidth;
    read_image->data = malloc(sizeof(struct pixel) * read_image->height * read_image->width);
    uint8_t padding = read_image->width % 4;

    //Считываем изображение
    for(uint64_t i=0; i <read_image->height;i++){
        if(!fread(&(read_image->data[i*read_image->width]),sizeof(struct pixel),read_image->width,in)) return FREAD_ERROR;
        fseek(in,padding,SEEK_CUR);
    }
    return result;
}
enum write_status to_bmp( FILE* out, struct image const* img ) {
    if(!out) return NULL_FILE_ERROR;
    struct bmp_header bmpHeader= initialize_bmp_header(img);
   if(!fwrite(&bmpHeader,sizeof(struct bmp_header),1,out)) return WRITE_ERROR;
    uint8_t padding = img->width % 4;
    for(size_t i = 0; i < img->height; i++) {
        if(!fwrite(&(img->data[i*img->width]), sizeof(struct pixel), img->width, out)) return WRITE_ERROR;
            fseek(out,padding,SEEK_CUR);
    }
    return WRITE_OK;
}
void print_read_status(enum read_status status ){
    char* message;
    switch (status) {
        case READ_OK:
            message=(char *)"File fread successfully";
            break;
        case READ_INVALID_TYPE:
            message=(char *)"Error with bit size bmp file";
            break;
        case FREAD_ERROR:
            message=(char *)"Wrong reading";
            break;
        default:
            message=(char *)"Some other problem related to the bmp file header";
            break;
    }
    printf("%s\n",message);
}
void print_write_status(enum write_status status ){
    char* message;
    if (status==WRITE_OK){ message=(char *)"File fwrite successfully";}
    else { message=(char *)"File fwrite error";}
    printf("%s\n",message);
}
void delete_image(struct image image){
    free(image.data);
    image.data=NULL;
}
